<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Metadata;

use Stringable;

/**
 * BundleInterface interface file.
 * 
 * This represents a bundle, a collection of records that are to be considered
 * to be eligible to crud.
 * 
 * @author Anastaszor
 */
interface BundleInterface extends Stringable
{
	
	/**
	 * Sets the id of this bundle.
	 *
	 * @param string $id
	 * @return static
	 */
	public function setId(?string $id) : static;
	
	/**
	 * Gets the id of this bundle.
	 *
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Sets the translated label for this bundle.
	 *
	 * @param string $category
	 * @param string $message
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $params
	 * @param ?string $language
	 * @return static
	 */
	public function setTLabel(string $category, string $message, array $params = [], ?string $language = null) : static;
	
	
	/**
	 * Sets the label of this bundle.
	 *
	 * @param ?string $label
	 * @return static
	 */
	public function setLabel(?string $label) : static;
	
	/**
	 * Gets the label of the bundle, according to the requested locale.
	 * 
	 * @return string
	 */
	public function getLabel() : string;
	
	/**
	 * @param array<integer|string, RecordInterface> $records
	 * @return static
	 */
	public function setRecords(array $records) : static;
	
	/**
	 * Gets the records that are enabled, indexed by url name.
	 * 
	 * @return array<string, RecordInterface>
	 */
	public function getEnabledRecords() : array;
	
	/**
	 * Gets whether the record and action with the given name is allowed.
	 * 
	 * @param ?string $recordId
	 * @param ?string $actionId
	 * @return boolean
	 */
	public function isAllowed(?string $recordId, ?string $actionId) : bool;
	
}
