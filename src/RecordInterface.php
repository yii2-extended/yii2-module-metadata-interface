<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Metadata;

use Stringable;
use yii\db\ActiveRecordInterface;

/**
 * RecordInterface interface file.
 * 
 * This represents metadata about a specific record in a bundle.
 * 
 * @author Anastaszor
 */
interface RecordInterface extends Stringable
{
	
	public const ACTION_INDEX = 'index';
	public const ACTION_VIEW = 'view';
	public const ACTION_CREATE = 'create';
	public const ACTION_UPDATE = 'update';
	public const ACTION_DELETE = 'delete';
	public const ACTION_SEARCH = 'search';
	
	/**
	 * Sets the id of this record class, set to null to use an generated one.
	 *
	 * @param ?string $id
	 * @return static
	 */
	public function setId(?string $id) : static;
	
	/**
	 * Gets the id of this record class.
	 *
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the class of this record.
	 *
	 * @return class-string<ActiveRecordInterface>
	 */
	public function getClass() : string;
	
	/**
	 * Sets the translated label for this record.
	 *
	 * @param string $category
	 * @param string $message
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>> $params
	 * @param ?string $language
	 * @return static
	 */
	public function setTLabel(string $category, string $message, array $params = [], ?string $language = null) : static;
	
	/**
	 * Sets the label of this record class, set to null to use a generated one.
	 * 
	 * @param ?string $label
	 * @return static
	 */
	public function setLabel(?string $label) : static;
	
	
	/**
	 * The label of the record class.
	 *
	 * @return string
	 */
	public function getLabel() : string;
	
	/**
	 * Sets a bootstrap icon name for the given record, set to null to use a
	 * default one.
	 * 
	 * @param ?string $icon
	 * @return static
	 */
	public function setBootstrapIconName(?string $icon) : static;
	
	/**
	 * Gets the bootstrap icon name of the given record.
	 *
	 * @return string
	 */
	public function getBootstrapIconName() : string;
	
	/**
	 * Gets the names of the fields in the record.
	 *
	 * @return array<integer, string>
	 */
	public function getFieldNames() : array;
	
	/**
	 * Sets the actions of this record.
	 *
	 * @param array<integer, string>|array<string, boolean> $actions $actions
	 * @return static
	 */
	public function setActions(array $actions) : static;
	
	/**
	 * Enables the index actions for this record class.
	 *
	 * @return static
	 */
	public function enableIndex() : static;
	
	/**
	 * Disables the index actions for this record class.
	 *
	 * @return static
	 */
	public function disableIndex() : static;
	
	/**
	 * Gets whether the index actions are enabled.
	 *
	 * @return boolean
	 */
	public function hasIndex() : bool;
	
	/**
	 * Enables the view actions for this record class.
	 *
	 * @return static
	 */
	public function enableView() : static;
	
	/**
	 * Disables the view actions for this record class.
	 *
	 * @return static
	 */
	public function disableView() : static;
	
	/**
	 * Gets whether the view actions are enabled.
	 *
	 * @return boolean
	 */
	public function hasView() : bool;
	
	/**
	 * Enables the create actions for this record class.
	 *
	 * @return static
	 */
	public function enableCreate() : static;
	
	/**
	 * Disables the create actions for this record class.
	 *
	 * @return static
	 */
	public function disableCreate() : static;
	
	/**
	 * Gets whether the create actions are enabled.
	 *
	 * @return boolean
	 */
	public function hasCreate() : bool;
	
	/**
	 * Enables the update actions for this record class.
	 *
	 * @return static
	 */
	public function enableUpdate() : static;
	
	/**
	 * Disables the update actions for this record class.
	 *
	 * @return static
	 */
	public function disableUpdate() : static;
	
	/**
	 * Gets whether the update action is enabled.
	 *
	 * @return boolean
	 */
	public function hasUpdate() : bool;
	
	/**
	 * Enables the delete actions for this record class.
	 *
	 * @return static
	 */
	public function enableDelete() : static;
	
	/**
	 * Disable the delete actions for this record class.
	 *
	 * @return static
	 */
	public function disableDelete() : static;
	
	/**
	 * Gets whether the delete actions are enabled.
	 *
	 * @return boolean
	 */
	public function hasDelete() : bool;
	
	/**
	 * Enable the search actions for this record class.
	 *
	 * @return static
	 */
	public function enableSearch() : static;
	
	/**
	 * Disable the search actions for this record class.
	 *
	 * @return static
	 */
	public function disableSearch() : static;
	
	/**
	 * Gets whether search actions are enabled.
	 *
	 * @return boolean
	 */
	public function hasSearch() : bool;
	
	/**
	 * Enables only non modification roles for this record class.
	 *
	 * @return static
	 */
	public function enableReadOnly() : static;
	
	/**
	 * Enables all roles except deletion for this record class.
	 *
	 * @return static
	 */
	public function enableModification() : static;
	
	/**
	 * Enables all the roles for this record class.
	 *
	 * @return static
	 */
	public function enableFullAccess() : static;
	
	/**
	 * Gets whether the action with the given name is allowed.
	 *
	 * @param string $actionId
	 * @return boolean
	 */
	public function isAllowed(?string $actionId) : bool;
	
}
