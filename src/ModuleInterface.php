<?php declare(strict_types=1);

/*
 * This file is part of the yii2-extended/yii2-module-metadata-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Yii2Extended\Metadata;

use Stringable;

/**
 * ModuleInterface interface file.
 * 
 * This interface is to be implemented by modules in order to give the crud
 * module all the metadata needed to be able to generate the crud interfaces
 * with all the due permissions.
 * 
 * @author Anastaszor
 */
interface ModuleInterface extends Stringable
{
	
	/**
	 * Gets the id of this module.
	 *
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the name of the bootstrap icon that is used by the module.
	 * 
	 * @return string
	 * @see https://icons.getbootstrap.com/
	 */
	public function getBootstrapIconName() : string;
	
	/**
	 * Gets the label of the module, according to the requested locale.
	 * 
	 * @return string
	 */
	public function getLabel() : string;
	
	/**
	 * Gets the bundles that are enabled, indexed by url name.
	 * This method is to postprocess some informations about bundles from the
	 * module, it gets its bundles from the getEnabledBundles() method.
	 * 
	 * @return array<string, BundleInterface>
	 */
	public function getBundles() : array;
	
	/**
	 * Gets the bundles that are enabled, indexed by url name.
	 * This method is to be implemented in modules.
	 * 
	 * @return array<string, BundleInterface>
	 */
	public function getEnabledBundles() : array;
	
	/**
	 * Gets whether the bundle, record and action with the given name is allowed.
	 * 
	 * @param ?string $bundleId
	 * @param ?string $recordId
	 * @param ?string $actionId
	 * @return boolean
	 */
	public function isAllowed(?string $bundleId, ?string $recordId, ?string $actionId) : bool;
	
}
