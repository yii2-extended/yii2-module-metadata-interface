# yii2-module-metadata-interface

A library to describe metadata about yii2 modules.

![coverage](https://gitlab.com/yii2-extended/yii2-module-metadata-interface/badges/main/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install yii2-extended/yii2-module-metadata-interface ^8`


## Basic Usage

This library is an interface library.

For a concrete implementation, see the [yii2-extended/yii2-module-metadata-object](https://gitlab.com/yii2-extended/yii2-module-metadata-object) library.


## License

MIT (See [license file](LICENSE)).
